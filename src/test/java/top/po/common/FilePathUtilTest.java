package top.po.common;

import junit.framework.TestCase;
import org.junit.Test;
import top.po.common.util.FilePathUtil;

public class FilePathUtilTest extends TestCase {

    @Test
    public void testRemoveMoreSeparator() {

        String path = "/sdf/s/ss////sdf/sdfw/qweqw";
        String s = FilePathUtil.removeMoreSeparator(path);
        System.out.println(s);
    }
}