package top.po.controller;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top.po.common.entity.FtpFile;
import top.po.common.util.FTPUtil;
import top.po.common.config.FtpConfig;

import java.io.File;
import java.io.IOException;

@Slf4j
@RequestMapping("/hello")
@RestController
public class HelloController {


    @Autowired
    private FTPUtil ftpUtil;
    @Autowired
    private FtpConfig ftpConfig;

    @GetMapping("/downloadFtp")
    public Object downloadDirs(String ftp) throws IOException {
        String rootPath = ftpConfig.getRootPath();
        if (StringUtils.isNotBlank(ftp)) {
            rootPath += "/" + ftp;
        }
        ftpUtil.downloadDirs(rootPath, "/opt/test");
        return null;
    }

    @GetMapping("/downloadFile")
    public Object downloadFile(String ftp) throws Exception {
        String rootPath = ftpConfig.getRootPath();
        if (StringUtils.isNotBlank(ftp)) {
            rootPath += "/" + ftp;
        }
        ftpUtil.downloadFile(rootPath, "E:/test/test.txt");
        return null;
    }


    @GetMapping("/showDirs")
    public Object showDirs(String ftp) throws Exception {
        String rootPath = ftpConfig.getRootPath();
        if (StringUtils.isNotBlank(ftp)) {
            rootPath += "/" + ftp;
        }
        FtpFile ftpFile = new FtpFile();
        ftpFile.setName(new File(rootPath).getName());
        ftpUtil.showDirs(ftpFile,rootPath, true);
        return ftpFile;
    }

    @GetMapping("/deleteFtp")
    public Boolean deleteFtp(String path) throws IOException {
//        ftpUtil.deleteDirs(path);
        return true;
    }
}
