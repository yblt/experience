package top.po.common.entity;

import lombok.Data;
import org.apache.commons.net.ftp.FTPFile;

import java.util.List;

@Data
public class FtpFile {

    private String name;
    private boolean isFile;
    private String path;
    private FTPFile ftpFile;
    List<FtpFile> children;
}
