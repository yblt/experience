package top.po.common.util;

/**
 * 文件路径转换
 *
 * @author 寄于念
 */
public class FilePathUtil {
    public static final String FILE_SEPARATOR = System.getProperty("file.separator");

    /**
     * 获取真实路径
     *
     * @param path 原路径
     * @return 真实路径
     */
    public static String getRealFilePath(String path) {
        return path.replace("/", FILE_SEPARATOR).replace("\\", FILE_SEPARATOR);
    }

    /**
     * 获取网络路径
     *
     * @param path 原路径
     * @return 网络路径
     */
    public static String getHttpURLPath(String path) {
        return path.replace("\\", "/");
    }

    /**
     * 去除多余 ///
     * @param path
     * @return
     */
    public static String removeMoreSeparator(String path) {
        return path.replaceAll("/+", "/");
    }
}
