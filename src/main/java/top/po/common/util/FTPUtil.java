package top.po.common.util;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import top.po.common.config.FtpConfig;
import top.po.common.entity.FtpFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;

/**
 * FTP连接工具
 *
 * @author yyh
 */
@Slf4j
@Component
public class FTPUtil {

    @Autowired
    private FtpConfig ftpConfig;

    private FTPClient ftpClient;

    private FTPUtil() {
    }


    public FTPClient getInstance(String ip, int port, String username, String password, int connectTimeout, String controlEncoding) {
        if (ftpClient != null) {
            return ftpClient;
        } else {
            try {
                ftpClient = new FTPClient();
                // 设置连接超时时间,5000毫秒
                ftpClient.setConnectTimeout(connectTimeout);
                // 设置中文编码集，防止中文乱码
                ftpClient.setControlEncoding(controlEncoding);
                // 连接FPT服务器,设置IP及端口
                ftpClient.connect(ip, port);
                //设置被动模式
                ftpClient.enterLocalPassiveMode();
                // 设置用户名和密码
                ftpClient.login(username, password);
                //获取响应状态
                int replyCode = ftpClient.getReplyCode();
                if (!FTPReply.isPositiveCompletion(replyCode)) {
                    log.error("连接" + ftpConfig.getIp() + "的FTP失败，用户名或者密码错误。");
                    //断开连接
                    ftpClient.disconnect();
                    ftpClient = null;
                    return null;
                } else {
                    log.info("连接" + ftpConfig.getIp() + "的FTP成功。");
                }
                //设置文件格式
//                ftpClient.setFileType(FTPClient.BINARY_FILE_TYPE);
                return ftpClient;
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        return null;
    }

    /**
     * 获取实例
     *
     * @return 实例
     */
    public FTPClient getInstance() {
        return getInstance(ftpConfig.getIp(), ftpConfig.getPort(), ftpConfig.getUsername(), ftpConfig.getPassword(), ftpConfig.getConnectTimeout(), ftpConfig.getControlEncoding());
    }

    /**
     * 关闭FTP方法
     */
    public void closeFTP() {
        //如果为空，结束
        if (ftpClient == null) {
            return;
        }
        try {
            //退出登录
            ftpClient.logout();
        } catch (Exception e) {
            log.error("FTP退出登录失败...");
        } finally {
            if (ftpClient.isConnected()) {
                try {
                    ftpClient.disconnect();
                    log.info("FTP断开连接...");
                } catch (IOException ioe) {
                    log.error("FTP关闭连接失败...");
                }
            } else {
                log.info("重置FTP对象...");
                ftpClient = null;
            }
        }
    }


    /**
     * FTP文件上传工具类
     *
     * @param sourcePath
     * @param targetPath
     * @return
     */
    public boolean uploadFile(String sourcePath, String targetPath) throws IOException {
        //为了防止程序部署在windows上面,ftp在Linux上面.导致路径不正确.统一使用网络路径'/'
        sourcePath = FilePathUtil.getHttpURLPath(sourcePath);
        targetPath = FilePathUtil.getHttpURLPath(targetPath);
        boolean flag = ftpClient.changeWorkingDirectory(targetPath);
        if (!flag) {
            String[] dirs = targetPath.split("");
            //遍历路径,找到没有创建的路径
            String createDirPath = "";
            for (String dir : dirs) {
                createDirPath = createDirPath + "/" + dir;
                boolean existDir = ftpClient.changeWorkingDirectory(dir);
                if (existDir) {
                    log.info("待创建文件夹:" + createDirPath);
                    log.info("文件夹已存在,准备创建下级文件夹...");
                } else {
                    ftpClient.mkd(createDirPath);
                    log.info("文件夹:" + createDirPath + ",创建已完成.准备创建下级文件夹...");
                }
            }
        }
        return false;
    }

    /**
     * 遍历
     * @param sourcePath 原路径
     * @param showGrandSon 是否展示所有子目录
     * @throws Exception
     */
    public void showDirs(FtpFile root,String sourcePath,boolean showGrandSon) throws Exception {
        String httpURLPath = FilePathUtil.getHttpURLPath(Paths.get(sourcePath).toString());
        log.info("httpURLPath:" + httpURLPath);
        boolean changeWorkingDirectory = ftpClient.changeWorkingDirectory(httpURLPath);
        if (!changeWorkingDirectory) {
            throw new Exception("目录不存在！");
        }
        FTPFile[] files = ftpClient.listFiles();

        ArrayList<FtpFile> ftpFiles = new ArrayList<>();
        //设置子目录
        root.setChildren(ftpFiles);
        for (FTPFile file : files) {
            FtpFile ftpFile = new FtpFile();
            ftpFile.setFtpFile(file);
            ftpFile.setName(file.getName());
            ftpFile.setFile(file.isFile());
            ftpFile.setPath(sourcePath);
            if (showGrandSon){
                if (file.isDirectory()){
                    showDirs(ftpFile,sourcePath+"/"+file.getName(),showGrandSon);
                }
            }
            ftpFiles.add(ftpFile);
        }
    }

    /**
     * 删除
     * @param sourcePath
     * @param targetPath
     * @throws IOException
     */

    /**
     * 下载文件
     *
     * @param sourcePath 文件路径
     * @param targetPath 目标路径
     * @throws Exception 异常
     */
    public void downloadFile(String sourcePath, String targetPath) throws Exception {
        String httpURLPath = FilePathUtil.getHttpURLPath(Paths.get(sourcePath).getParent().toString());
        log.info("htppURLPath:" + httpURLPath);
        boolean changeWorkingDirectory = ftpClient.changeWorkingDirectory(httpURLPath);
        if (!changeWorkingDirectory) {
            throw new Exception("文件夹:" + httpURLPath + ",切换失败!");
        }
        //下载文件
        String fileName = new File(sourcePath).getName();
        InputStream inputStream = ftpClient.retrieveFileStream(fileName);
        Files.copy(inputStream, Paths.get(targetPath), StandardCopyOption.REPLACE_EXISTING);
        //需要使用ftpClient.getReply()，将这次请求进行消费。不然只能下载第一个文件 ，
        inputStream.close();
        ftpClient.completePendingCommand();
        log.info("下载状态：" + ftpClient.getReplyCode());

    }

    /**
     * 下载文件夹
     *
     * @param sourcePath 需要下载的路径
     * @param targetPath 目标路径
     */
    public void downloadDirs(String sourcePath, String targetPath) throws IOException {
        String httpURLPath = FilePathUtil.getHttpURLPath(Paths.get(sourcePath).toString());
        log.info("httpURLPath:" + httpURLPath);
        boolean changeWorkingDirectory = ftpClient.changeWorkingDirectory(httpURLPath);
        String replyString = ftpClient.getReplyString();
        log.info("ftp切换目录状态:"+ftpClient.getReplyCode()+",ftp响应结果："+replyString);
        if (changeWorkingDirectory) {
            //获取指定目录下文件文件对象集合
            FTPFile[] files = ftpClient.listFiles();
            for (FTPFile file : files) {
                //判断为txt文件则解析
                if (file.isFile()) {
                    String fileName = file.getName();
                    log.info("下载文件名称：" + sourcePath + "/" + fileName);
                    ftpClient.changeWorkingDirectory(sourcePath);
                    //下载文件
                    boolean exists = Files.deleteIfExists(Paths.get(targetPath + "/" + fileName));
                    if (exists){
                        log.info("文件:"+fileName+",已经存在,先删除。");
                    }
                    FileOutputStream fileOutputStream = new FileOutputStream(targetPath + "/" + fileName);
                    log.info("获取文件流响应："+ftpClient.getReplyString());
                    ftpClient.retrieveFile(fileName,fileOutputStream);
                    log.info("下载状态：" + ftpClient.getReplyCode());
                    fileOutputStream.close();
                } else if (file.isDirectory()) {
                    String fileName = file.getName();
                    String path = targetPath + "/" + fileName;
                    if (!Files.exists(Paths.get(path))) {
                        Files.createDirectory(Paths.get(path));
                    }
                    downloadDirs(sourcePath + "/" + fileName, path);
                }
            }
        } else {
            log.error(sourcePath + ",在FTP中不存在");
        }
    }


}