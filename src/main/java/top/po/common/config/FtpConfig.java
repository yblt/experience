package top.po.common.config;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;

/**
 * Ftp配置
 * 可修改配置文件
 * ftp在不同的操作系统，需要设置字符集进行更正。
 * @author yyh
 *
 */
@Slf4j
@Data
@PropertySource("classpath:application-ftp.properties")
@ConfigurationProperties(prefix = "ftp")
public class FtpConfig {
    /**
     * ftp服务器ip地址
     */
    private  String ip="localhost";

    /**
     * 端口号
     */
    private int port=21;

    /**
     * 用户名
     */
    private String username="admin";

    /**
     * 密码
     */
    private String password="admin";

    /**
     * 超时时间
     */
    private int connectTimeout=50000;

    /**
     * 字符集
     */
    private String controlEncoding="utf-8";

    /**
     * ftp根路径
     */
    private String rootPath;
}
