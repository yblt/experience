package top.po.framework;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.net.ftp.FTPClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import top.po.common.util.FTPUtil;
import top.po.common.config.FtpConfig;

import javax.annotation.PostConstruct;

@Slf4j
@Configuration
@EnableConfigurationProperties(FtpConfig.class)
public class SystemConfiguration {


    private final static String sdf="sdf";
    @Autowired
    private FTPUtil ftpUtil;
    @PostConstruct
    public void init() {
        FTPClient ftpClient = ftpUtil.getInstance();
        if (ftpClient==null){
            log.error("ftpClient为空！");
        }
    }
}
